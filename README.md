A basic base64 encoder / decoder for Rust.
==========================================

### STATUS:
* master: [![pipeline status](https://gitlab.com/jeremymreed/base64-lib/badges/master/pipeline.svg)](https://gitlab.com/jeremymreed/base64-lib/commits/master)
* develop: [![pipeline status](https://gitlab.com/jeremymreed/base64-lib/badges/develop/pipeline.svg)](https://gitlab.com/jeremymreed/base64-lib/commits/develop)

==========================================

1. [Usage](https://gitlab.com/jeremymreed/base64-lib#usage)
	* [Import](https://gitlab.com/jeremymreed/base64-lib#import)
	* [Encode](https://gitlab.com/jeremymreed/base64-lib#encode)
	* [Decode](https://gitlab.com/jeremymreed/base64-lib#usage)
	* [Encode With Custom Alphabet](https://gitlab.com/jeremymreed/base64-lib#encode_with_custom_alphabet)
	* [Decode With Custom Alphabet](https://gitlab.com/jeremymreed/base64-lib#decode_with_custom_alphabet)
2. [Notes](https://gitlab.com/jeremymreed/base64-lib#notes)
3. [License](https://gitlab.com/jeremymreed/base64-lib#license)

### Usage:

## Import:

At the top of the file:
``` Rust
extern crate base64_lib;
```

## Encode:

# encode(&Vec<u8>) -> String

``` Rust
let input_vector: Vec<u8> = String::from("Hello World").into_bytes();
let result_string: String = base64_lib::encode(&input_vector);
```

## Decode:

# decode(&String) -> Vec<u8>

``` Rust
let input_string: String = String::from("SGVsbG8gV29ybGQ=");
let result_vector: Vec<u8> = base64_lib::decode(&input_string);
```

## Encode with custom alphabet:

# encode_with_alphabet(&Vec<u8>, &String) -> Vec<u8>

``` Rust
let input_vector: Vec<u8> = String::from("Hello World").into_bytes();
let alphabet: String = String::from("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
let result_string: String = base64_lib::encode_with_alphabet(&input_vector, &alphabet);
```

## Decode with custom alphabet:

# decode_with_alphabet(&String, &String) -> Vec<u8>

```
let input_string: String = String::from("SGVsbG8gV29ybGQ=");
let alphabet: String = String::from("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
let result_vector: Vec<u8> = base64_lib::decode_with_alphabet(&input_string, &alphabet);
```

### Notes:

- When using a custom alphabet, be sure there are 64 unique characters in the string.

### License:

# Licensed under the MIT License.